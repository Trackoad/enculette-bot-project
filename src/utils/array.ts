export class ArrayUtils<T> extends Array<T> {

    getFirstElement(): T | undefined {
        if(!this.length){
            return undefined;
        }
        return this[0];
    }

}