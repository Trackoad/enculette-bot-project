import { RegExpModel } from "../model/regexp-model"

export class RegExpService {

    static readonly REGEXP_NUMBER = new RegExpModel(/^[0-9]+$/, 'un nombre');

    static readonly REGEXP_LETTER = new RegExpModel(/^[A-Za-z]+$/, 'une chaîne de caractères');
}