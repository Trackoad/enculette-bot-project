import { ConfigService } from "./config-service";

export class LoggerService {

    static log(message?: any) {
        console.log(message);
    }

    static error(message: string, exp?: Error) {
        if(ConfigService.instance.configBase.debug && exp){
            console.error(exp);
        }
        console.error(message);
    }

    static app(message: string) {
        console.log('[APP]', message);
    }
}