import confBase from '../config/config-data.json';
import { ConfigModel } from '../model/config-model.js';

export class ConfigService {

    private _configBase: ConfigModel;
    get configBase(): ConfigModel {
        return this._configBase;
    }

    private static _instance: ConfigService;
    static get instance(): ConfigService {
        return ConfigService._instance ? ConfigService._instance : ConfigService._instance = new ConfigService();
    }

    private constructor() { 
        this._configBase = confBase;
    }
}