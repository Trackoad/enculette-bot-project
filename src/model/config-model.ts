export class ConfigModel {

    readonly token!: string;

    readonly debug?: boolean;

    readonly commandStarter?: string;
}