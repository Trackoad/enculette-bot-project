import { CommandInterfaceModel } from "../command-model";
import Discord = require('discord.js');
import { RoleModel } from "../role-model";
import { RoleService } from "../../services/role-service";
import { RegExpModel } from "../regexp-model";

export class ClearCommand implements CommandInterfaceModel {

    permissions(): RoleModel[] {
        return [
            RoleService.ROLE_ADMIN
        ];
    }

    canDeleteMessage(): boolean {
        return true;
    }

    name(): string {
        return 'clear';
    }

    description(): string {
        return 'Clear channel';
    }

    argsRegex(): RegExpModel[] {
        return [];
    }
    execute(args: String[], message: Discord.Message): void {
        message.channel.fetchMessages().then(messages => message.channel.bulkDelete(messages));
    }
}