import { CommandInterfaceModel } from "../command-model";
import { HelpCommandModel } from "./help-command-model";
import { ClearCommand } from "./clear-command";


export const CommandsList: Array<CommandInterfaceModel> = [
    HelpCommandModel,
    ClearCommand,
].map(command => Object.create(command.prototype));